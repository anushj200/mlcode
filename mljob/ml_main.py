from fastapi import Depends, APIRouter, HTTPException, Request
from . import schemas, ml_update
from util import Scheduler
import yaml
from datetime import datetime

router = APIRouter()

@router.post("/run/ml")
async def standard_ml(request: Request, input: schemas.MLBaseShow):
    trackking = Scheduler.readproperty()
    if trackking["runing_job"]:
        output = {"status":"Cannot process the request due to job already in running state"}
    else:
        now = datetime.now()
        trigger_time = now.strftime("%d/%m/%Y %H:%M:%S")
        refid = ml_update.random_string_generator()
        trackking["runing_job"] = True
        trackking["job_id"] = refid
        trackking["last_run"] = trigger_time
        trackking["job_status"] = "About_To_Start"
        Scheduler.writeproperty(trackking)
        output = {"status": "ML Project triggered", "refid": refid}
    return output


@router.get("/track/ml")
async def standard_ml(request: Request):
    trackking = Scheduler.readproperty()
    return trackking
