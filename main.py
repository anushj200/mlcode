from fastapi import FastAPI
import uvicorn
from mljob import ml_main
from util import Scheduler
app = FastAPI()


@app.get("/")
async def root():
    return {"message": "ML Project"}

app.include_router(ml_main.router, prefix="/runjob", tags=["ML"],
               responses={404: {"description": "Not found"}}, )


Scheduler.start()

if __name__ == '__main__':
    uvicorn.run(app, port=8080, host='0.0.0.0')
