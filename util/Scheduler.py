import requests
import yaml
from apscheduler.schedulers.background import BackgroundScheduler
from mljob import ml_worker

def readproperty():
    with open(r'/home/ec2-user/ml/util/job_tracker.yaml') as file:
        mlvar = yaml.full_load(file)
        return mlvar

def writeproperty(data):
    with open('/home/ec2-user/ml/util/job_tracker.yaml','w') as file:
        file.write( yaml.dump(data, default_flow_style=False))
        return True

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(ml_worker.scheduler_ml_polling, 'interval', minutes=1)
    scheduler.start()
